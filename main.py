import math
import random

from kivy import Config
from kivy.app import App
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.properties import NumericProperty, ColorProperty, ObjectProperty, BooleanProperty, StringProperty, \
    ListProperty
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.widget import Widget

Config.set('graphics', 'resizable', 0)
Config.write()

ordered_colors = ((237 / 256, 28 / 256, 36 / 256),
                  (0, 166 / 256, 81 / 256),
                  (46 / 256, 49 / 256, 146 / 256),
                  (1, 242 / 256, 0),
                  (102 / 256, 45 / 256, 145 / 256),
                  (242 / 256, 101 / 256, 34 / 256),
                  (236 / 256, 0, 140 / 256),
                  (0, 174 / 256, 239 / 256))


def lighten_color(color):
    return [min(x + 0.15, 1.0) for x in color]


def darken_color(color):
    return [max(x - 0.15, 0.0) for x in color]


class SuperSimonButton(Widget):
    id = NumericProperty()
    angle = NumericProperty()
    color = ColorProperty()
    rotate_origin = ObjectProperty(None)

    def __init__(self, base_color, pos_hint, size_hint, **kwargs):
        self.base_color = base_color
        super(SuperSimonButton, self).__init__(**kwargs)

        self.color = darken_color(base_color)
        self.pos_hint = pos_hint
        self.size_hint = size_hint

    def collide_point(self, x: float, y: float):
        """As far as we know, Kivy only applies rotation to a Widget's canvas. We must then override the collide_point()
        method to take this rotation in account when checking for touch collision."""
        touch_point = (x, y)
        wx, wy = Window.center

        def rotate_point(point_x: float, point_y: float):
            """Applies the Widget's rotation to the given point's coordinates."""
            angle = math.radians(self.angle)
            temp_x = point_x - wx
            temp_y = point_y - wy
            point_x = temp_x * math.cos(angle) - temp_y * math.sin(angle) + wx
            point_y = temp_x * math.sin(angle) + temp_y * math.cos(angle) + wy
            return point_x, point_y

        def triangle_area(point_1: tuple[float, float], point_2: tuple[float, float], point_3: tuple[float, float]):
            """Returns the area of the triangle formed by the 3 given points."""
            return abs((point_1[0] * (point_2[1] - point_3[1]) +
                        point_2[0] * (point_3[1] - point_1[1]) +
                        point_3[0] * (point_1[1] - point_2[1])) / 2)

        # This algorithm allows us to determine if the touch point is in the rectangle.
        # If the point is in the rectangle then the sum of the areas of the four triangles created from the touch point
        # and each rectangle side is equal (or nearly equal due to floating-point numbers) to the rectangle's area.
        points = (rotate_point(self.x, self.top), rotate_point(self.right, self.top),
                  rotate_point(self.right, self.y), rotate_point(self.x, self.y))
        triangle_areas = (triangle_area(points[0], touch_point, points[3]),
                          triangle_area(points[3], touch_point, points[2]),
                          triangle_area(points[2], touch_point, points[1]),
                          triangle_area(touch_point, points[1], points[0]))

        return sum(triangle_areas) < (self.width * self.height) + 0.01

    def on_touch_down(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.parent.button_click(self)
        return super(SuperSimonButton, self).on_touch_down(touch)

    def switch_on(self, dt=None):
        self.color = lighten_color(self.base_color)

    def switch_off(self, dt=None):
        self.color = darken_color(self.base_color)


class SuperSimonGame(RelativeLayout):
    # Final
    difficulty = StringProperty('Easy')
    randomize_buttons = BooleanProperty(False)
    high_score = NumericProperty(0)

    # Reset at the end of the round
    game_state = NumericProperty(0)
    score = NumericProperty(-1)
    buttons: list[SuperSimonButton] = ListProperty()
    button_sequence: list[SuperSimonButton] = ListProperty()
    current_button_in_sequence: int = NumericProperty(0)

    def __init__(self, settings_callback, reset_callback, game_speed, **kwargs):
        super(SuperSimonGame, self).__init__(**kwargs)
        self.settings_callback = settings_callback
        self.reset_callback = reset_callback
        self.ids.button_settings.on_press = self.open_settings
        self.ids.button_restart_game.on_press = self.reset_game
        self.speed_ratio = 1.25 if game_speed == 'Slow' else 0.4 if game_speed == 'Fast' else 1
        self.generate_buttons()

    def generate_buttons(self):
        n_buttons = 4 if self.difficulty == 'Easy' else 6 if self.difficulty == 'Normal' else 8
        colors = list(ordered_colors)
        if self.randomize_buttons is True:
            random.shuffle(colors)

        if self.buttons:
            for button in self.buttons:
                self.remove_widget(button)
            self.buttons = []

        for i in range(n_buttons):
            button = SuperSimonButton(id=i, angle=(360 * (i - 0.5)) // n_buttons, base_color=colors[i],
                                      pos_hint={'center_x': 0.5, 'center_y': 0.85}, size_hint=(0.15, 0.1))
            self.add_widget(button)
            self.buttons.append(button)
        self.button_sequence = []
        self.add_button_to_sequence()

    def add_button_to_sequence(self):
        """Appends a new button to the button sequence (called after a round is won or when the game starts)"""
        self.current_button_in_sequence = 0
        self.button_sequence.append(random.choice(self.buttons))

    def button_click(self, button: SuperSimonButton):
        """Starts the game if it has not started yet
        or checks if the clicked button is the right one if in the play phase."""
        if self.game_state == 0:
            self.start_show_sequence_phase()
        elif self.game_state == 2:
            if self.button_sequence[self.current_button_in_sequence] is button:
                self.current_button_in_sequence += 1
                Clock.schedule_once(button.switch_on)
                Clock.schedule_once(button.switch_off, 0.25)
                if self.current_button_in_sequence == len(self.button_sequence):
                    Clock.schedule_once(self.win_round, 0.5)
            else:
                self.game_over()

    def start_show_sequence_phase(self):
        """Shows which buttons to click in the play phase one by one."""
        if self.game_state == 0:
            self.score = 0
        self.game_state = 1
        for i, button in enumerate(self.button_sequence):
            Clock.schedule_once(button.switch_on, 0.5 + i * self.speed_ratio)
            Clock.schedule_once(button.switch_off, 0.5 + (i + 0.7) * self.speed_ratio)
        Clock.schedule_once(self.start_play_phase, 0.5 + len(self.button_sequence) * self.speed_ratio)

    def start_play_phase(self, dt=None):
        self.game_state = 2

    def win_round(self, dt=None):
        self.score += 1
        if self.score > self.high_score:
            self.high_score = self.score
        self.add_button_to_sequence()
        self.start_show_sequence_phase()

    def game_over(self):
        self.game_state = 4
        for i in range(3):
            Clock.schedule_once(self.button_sequence[self.current_button_in_sequence].switch_on, i * 2)
            Clock.schedule_once(self.button_sequence[self.current_button_in_sequence].switch_off, i * 2 + 1.5)
        Clock.schedule_once(self.reset_game, 6)

    def reset_game(self, dt=None):
        self.reset_callback()

    def open_settings(self):
        """Opens the settings, but only if no game has started, as it would reset it."""
        if self.game_state == 0:
            self.settings_callback()


class SuperSimonGameHolder(AnchorLayout):
    """Holds one instance of SuperSimonGame"""
    pass


class SuperSimonApp(App):
    game_holder = None
    game: SuperSimonGame = None

    def build_config(self, config):
        config.setdefaults('simon', {
            'difficulty': 'Easy',
            'game_speed': 'Slow',
            'randomize_buttons': '0',
            'high_score': '0'
        })

    def build_settings(self, settings):
        json_data = """[
    { "type": "title",
      "title": "Super Simon" },
    { "type": "options",
      "title": "Difficulty",
      "desc": "Button amount (4, 6, 8)",
      "section": "simon",
      "key": "difficulty",
      "options": ["Easy", "Normal", "Hard"] },
    { "type": "options",
      "title": "Game speed",
      "desc": "Speed at which the buttons will flash",
      "section": "simon",
      "key": "game_speed",
      "options": ["Slow", "Normal", "Fast"] },
    { "type": "bool",
      "title": "Random button colors",
      "section": "simon",
      "key": "randomize_buttons" }
]"""
        settings.add_json_panel('Super Simon', self.config, data=json_data)

    def on_config_change(self, config, section, key, value):
        if section == 'simon':  # Resets the game if its configuration has changed
            self.reset_game()

    def build(self):
        self.create_settings()

        self.game_holder = SuperSimonGameHolder()
        self.reset_game()
        return self.game_holder

    def reset_game(self):
        if self.game:
            if self.game.score > self.config.getint('simon', 'high_score'):
                self.config.set('simon', 'high_score', str(self.game.score))
                self.config.write()
            self.game_holder.remove_widget(self.game)
            del self.game
        self.game = SuperSimonGame(settings_callback=self.open_settings, reset_callback=self.reset_game,
                                   difficulty=self.config.get('simon', 'difficulty'),
                                   game_speed=self.config.get('simon', 'game_speed'),
                                   randomize_buttons=self.config.getboolean('simon', 'randomize_buttons'),
                                   high_score=self.config.getint('simon', 'high_score'))
        self.game_holder.add_widget(self.game)


if __name__ == '__main__':
    SuperSimonApp().run()
